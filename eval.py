import sys
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '2'

import torchvision.datasets as datasets
import torchvision.transforms as transforms

import torch
from torch import nn
from torch.utils.data  import DataLoader
from dataset import ACRDataset, ToTensor, ToTensor3Channel, Rescale
from model_def import Encoder, ResNetEncoder

import yaml
import numpy as np
from matplotlib import pyplot as plt

device = "cuda"
# device = "cpu"


def sim_test(model_path, n_test):

    model_name = os.path.splitext(os.path.basename(model_path))[0]
    outdir = os.path.join('./output', model_name)
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    model = torch.load(model_path)

    tforms = transforms.Compose([Rescale(),ToTensor()])
    # tforms = transforms.Compose([Rescale(),ToTensor3Channel()])
    data_eval = ACRDataset(n_test, transform=tforms)

    kwargs = {"num_workers": 1, "pin_memory": True} if (device == "cuda") else {}

    batch_size = n_test


    eval_dataloader = DataLoader(data_eval, batch_size = batch_size, shuffle = True, **kwargs)

    # X, y = data_eval["image"], data_eval["feature_vec"]
    # print(y)

    with torch.no_grad():
        # M: this is not the best way to do this, sorry
        count = 0
        for batch in eval_dataloader:

            X, y = batch["image"], batch["feature_vec"]

            X = X.to(device)
            y = y.to(device)

            pred = model(X)
            pred = pred.to(device)

            diff = np.abs(y.detach().cpu().numpy() - pred.detach().cpu().numpy())
            # print(diff)
            count += 1

    # this is embarassing sorry it's too late for me to think 
    assert(count==1)
    pos_x, pos_y, fov, rot = diff[:,0], diff[:,1], diff[:,2], diff[:,3]

    print('pos_x')
    print(np.round(np.mean(pos_x), 2), np.round(np.std(pos_x), 2))
    print('pos_y')
    print(np.round(np.mean(pos_y), 2), np.round(np.std(pos_y), 2))
    print('fov')
    print(np.round(np.mean(fov), 2), np.round(np.std(fov), 2))
    print('rot')
    print(np.round(np.mean(rot), 2), np.round(np.std(rot), 2))
    # out = torch.cat(diff_list)
    # print(out[:,1].float().mean())



# Call our main function
if __name__=="__main__":

    model_path = './model_09.pt'
    n_test = 100
    sim_test(model_path, n_test)


    acr_test_dir = './data/dl-acr-test'
    ref_path = './sample_cases.yaml'
    with open(ref_path, 'r') as file:
        case_dict = yaml.safe_load(file)

    acr_test(model_path, acr_test_dir, case_dict)
    