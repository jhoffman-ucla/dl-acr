import sys
import os
import math
import numpy as np
from numpy import random
import matplotlib.pyplot as plt

import torch
from torchmetrics.functional import mean_squared_error

from analysis_tools import CircleROI

def RandRange(val_min, val_max):
    return random.rand() * (val_max - val_min) + val_min

def RandomPhantom():
    # Generate random inputs for our phantom
    pos_min, pos_max = (-50, 50)
    # fov_min, fov_max = (300, 300)
    fov_min, fov_max = (200, 510)
    # rot_min, rot_max = (0, 0)
    rot_min, rot_max = (-50, 65)

    pos_x = RandRange(pos_min, pos_max)
    pos_y = RandRange(pos_min, pos_max)
    fov = RandRange(fov_min, fov_max)
    rot = RandRange(rot_min, rot_max)
    flip = False #bool(round(random.rand()))

    feature_vec = [pos_x, pos_y, fov, rot]

    acr = ACRPhantom()
    acr.SetPosition(pos_x, pos_y)
    acr.SetRotation(rot)
    acr.SetFlip(flip)

    r = RenderBuffer()
    r.fov = (fov, fov)

    return acr.Render(r), (pos_x, pos_y, fov, rot, flip)

def ExactPhantom(pos_x, pos_y, fov, rot, flip, dim = 256):
    acr = ACRPhantom()
    acr.SetPosition(pos_x, pos_y)
    acr.SetRotation(rot)
    acr.SetFlip(flip)
    r = RenderBuffer(fov = fov, dim = dim)
    return acr.Render(r)

class RenderBuffer:
    fov = [500.0, 500.0]
    dim = [256,256]
    center = [0.0, 0.0]
    buff = None
    bg = -1000

    def __init__(self, fov = 500, dim = 256):
        self.fov = [fov,fov]
        self.dim = [dim, dim]
        self.buff = self.bg * np.ones((self.dim[0],self.dim[1]), dtype=np.single)

    def GetPositionsArray(self):
        pixel_dim = np.divide(self.fov, self.dim)
        x = pixel_dim[0]*(np.arange(self.dim[0], dtype=np.single) - (self.dim[0]-1.0)/2.0) - self.center[0]
        y = pixel_dim[1]*(np.arange(self.dim[1], dtype=np.single) - (self.dim[1]-1.0)/2.0) - self.center[1]
        return np.meshgrid(x,y)

    def Set(self, mask, val):
        self.buff[mask] = val

    def Clear(self, clear_val = None):

        if clear_val is not None:
            self.buff = clear_val * np.ones((self.dim[0],self.dim[1]), dtype=np.single)
        else:
            self.buff = self.bg * np.ones((self.dim[0],self.dim[1]), dtype=np.single)
        
class Transform:
    parent = None
    rotation = 0
    position = (0,0)
    flip = False
    children = []

    def __init__(self, position, rotation, flip):
        self.position = position
        self.rotation = rotation
        self.flip = flip
        self.children = []

    def SetFlip(self, val):
        self.flip = val

    # Note: deliberately not adding remove methods for
    #       parents/children b/c I can't see a good reason to have
    #       them for this intended use case.
    def SetParent(self,p):
        if p == self:
            print("ERROR: object cannot parent itself")
            sys.exit(1)

        self.parent = p
        p.children.append(self)

    # Get true position in object coordinates including rotation
    # Note: does not use parent transforms
    def GetPosition(self):
        # Rotation must be in degrees
        x = self.position[0]
        y = self.position[1]
        return (x, y)

    def GetWorldPosition(self):

        # First, we have to apply the local transform
        x, y = self.GetPosition()

        # Next, recursively apply parent tranforms until we get None
        curr_parent = self.parent
        while curr_parent != None:
            rad = (math.pi/180) * curr_parent.rotation

            # Rotate
            tmp_x = x * math.cos(rad) - y * math.sin(rad)
            tmp_y = x * math.sin(rad) + y * math.cos(rad)
            
            if curr_parent.flip:
                tmp_x = -tmp_x

            # Translate
            x = tmp_x + curr_parent.position[0]
            y = tmp_y + curr_parent.position[1]


            curr_parent = curr_parent.parent

        return (x, y)

class Object(Transform):
    name = "unnamed object"
    radius = 20.0
    HU = 100
    analysis_fn = None

    def __init__(self, name):
        super().__init__((0,0), 0, False)
        self.name = name

    def __str__(self):
        s = "Object: {}\nParent: {}\nChildren: {}\n".format(self.name, self.parent, self.children)
        return s
    
    def SetPosition(self, x, y):
        self.position = (x, y)

    def SetRotation(self, r):
        self.rotation = r

    def Render(self, render_buff=None):

        if render_buff == None:
            render_buff = RenderBuffer()

        X,Y = render_buff.GetPositionsArray()

        # Render self into renderbuffer
        x, y = self.GetWorldPosition()

        X_off = X-x;
        Y_off = Y-y;

        mask = ((np.multiply(X_off,X_off) + np.multiply(Y_off, Y_off)) < (self.radius*self.radius))

        render_buff.Set(mask, self.HU)

        # Recurse into childen and render
        [ch.Render(render_buff) for ch in self.children]

        return np.expand_dims(render_buff.buff.astype(np.single), axis=0)

    def Analyze(self, image, render_buff):

        result = {}
        
        if self.analysis_fn != None:
            #result[self.name] = self.analysis_fn() # analysis_fn must return dictionary
            result = self.analysis_fn(image, render_buff)

        for i, c in enumerate(self.children):
            c_result = c.Analyze(image, render_buff)
            if c_result != None:
                result[c.name] = c_result

        if len(result)==0:
            return None

        return result

class HUModule(Object):
    def __init__(self, name, x, y, r, HU, limits = None, segmentation_id = 1):
        self.name = name
        self.SetPosition(x,y)
        self.radius = r
        self.HU = HU
        self.limits = limits
        self.segmentation_id = segmentation_id
        
    def analysis_fn(self, image, render_buff):
        # Draw self into render buff to get a mask, apply to image
        #self.Render(render_buff = render_buff)
        if image is None or render_buff is None:
            return {"dummy_mean": self.HU, "dummy_std": 0}

        image = np.squeeze(image)
        
        # Need to extract an ROI w/specific radius != radius of object
        # So we cant just have the object render itself into the scene,
        # we need to "draw" an ROI centered on the position.
        x, y = self.GetWorldPosition()
        r = 0.6*self.radius
        mask = CircleROI(x, y, r, render_buff)

        #plt.imshow(image)
        #plt.imshow(mask, cmap = "jet", vmin = 0, vmax = 1, alpha = 0.5)
        #plt.show()
        
        render_buff.Set(mask, self.segmentation_id)
        
        vals = image[mask]
        mean = np.mean(vals)
        std = np.std(vals)

        data = {"mean": mean, "std": std, "seg_id": self.segmentation_id}

        if self.limits is not None:
            data["allowed_range"] = self.limits
            passes = (mean >= self.limits[0]) and (mean <= self.limits[1])
            if passes:
                data["result"] = "PASS"
            else:
                data["result"] = "FAIL"
            
        return data
        

class ACRPhantom(Object):
    acrylic = None
    bone = None
    polyethylene = None
    air = None
    h20 = None
    
    def __init__(self):

        super().__init__("acr_phantom")

        self.radius = 100;
        self.HU = 0;

        # Alignment beads
        bead_1 = Object("alignment bead 1")
        bead_1.radius = 1
        bead_1.HU = 1000
        bead_1.SetParent(self)
        bead_1.SetPosition(100, 0)

        bead_2 = Object("alignment bead 2")
        bead_2.radius = 1
        bead_2.HU = 1000
        bead_2.SetParent(self)
        bead_2.SetPosition(-100, 0)

        bead_3 = Object("alignment bead 3")
        bead_3.radius = 1
        bead_3.HU = 1000
        bead_3.SetParent(self)
        bead_3.SetPosition(0, 100)

        bead_4 = Object("alignment bead 4")
        bead_4.radius = 1
        bead_4.HU = 1000
        bead_4.SetParent(self)
        bead_4.SetPosition(0, -100)

        # Main objects
        acrylic = HUModule("acrylic", -45, 45, 12.5, 120, limits = [110, 135], segmentation_id = 1)
        acrylic.SetParent(self)

        polyeth = HUModule("polyethylene", -45, -45, 12.5, -95, limits = [-107, -84], segmentation_id = 2)
        polyeth.SetParent(self)
        
        air = HUModule("air", 45, 45, 12.5, -1000, limits = [-1005, -970], segmentation_id = 3)
        air.SetParent(self)
        
        bone = HUModule("bone", 45, -45, 12.5, 955, limits = [850, 970], segmentation_id = 4)
        bone.SetParent(self)

        water = HUModule("water", -60, 0, 12.5, 0, limits = [-7, 7], segmentation_id = 5)
        water.SetParent(self)
        
if __name__=='__main__':

    phantom = ACRPhantom()

    print(phantom.Analyze(None, None))

    ## Render the phantom
    #r = RenderBuffer()
    #r.fov = (250,250)
    #r.center = (0,0)
    #
    #phantom.SetPosition(0,0)
    #rndr_0 = phantom.Render(r)
    #plt.subplot(1, 3, 1)
    #plt.imshow(rndr_0)
    #r.Clear()
    #
    #phantom.SetRotation(45)
    #phantom.SetPosition(35,0)
    #rndr_1 = phantom.Render(r)
    #plt.subplot(1, 3, 2)
    #plt.imshow(rndr_0)
    #r.Clear()
    #
    #phantom.SetFlip(True)
    #rndr_2 = phantom.Render(r)
    #plt.subplot(1,3,3)
    #plt.imshow(rndr_2)
    #r.Clear()

    plt.show()