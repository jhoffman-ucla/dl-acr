import numpy as np
import matplotlib.pyplot as plt


############## IMPORTANT!!!!! ##################
# All functions in this mini plotting library should generate and
# return a matplotlib.fig. This is because we want to let the user
# configure whether or not figures should be saved or shown when
# generated.

def PlotEpochSaliencyMap(image, saliency_map, epoch_idx, loss=None):
    fig = plt.figure(figsize=(8,4), dpi=100)

    ax = plt.subplot(1,2,1)
    ax.set(position=[0, 0, 0.5, 1])
    ax.set_axis_off()

    plt.imshow(np.squeeze(image))

    ax2 = plt.subplot(1,2,2)
    ax2.set(position=[0.5, 0, 0.5, 1])
    ax2.set_axis_off()

    ax2.text(128, 10, "epoch: {}".format(epoch_idx), color="white", horizontalalignment='center')

    if loss is not None:
        ax2.text(128, 248, "loss: {}".format(round(loss,3)), color="white", horizontalalignment='center')

    plt.imshow(saliency_map, cmap="hot")
    return fig

def PlotEpochSaliencyMapWithLoss(image, saliency_map, epoch_idx, training_loss, validation_loss):

    fig = plt.figure(figsize=(12,4), dpi=100)

    ax1 = plt.subplot(1,3,1)
    ax1.set(position=[0, 0, 0.333, 1])
    ax1.set_axis_off()

    plt.imshow(np.squeeze(image))

    ax2 = plt.subplot(1,3,2)
    ax2.set(position=[.333, 0, 0.333, 1])
    ax2.set_axis_off()

    plt.imshow(saliency_map, cmap="hot")

    curr_val_loss = validation_loss[-1]
    curr_train_loss = training_loss[-1]

    label_train = "training loss: %.2f" % curr_train_loss
    label_val = "validation loss: %.2f" % curr_val_loss

    ax2.text(128, 10, "epoch: {}".format(epoch_idx), color="white", horizontalalignment='center')

    ax3 = plt.subplot(1,3,3)
    ax3.set(position=[.667, 0, 0.333, 1])

    # (I think) we're mostly interested in the validation loss; thus
    # we plot both losses but make the training loss a little
    # transparent
    line_train = ax3.plot(training_loss, color="paleturquoise", alpha = 0.25)
    line_val = ax3.plot(validation_loss, color="forestgreen", alpha = 0.5)

    #ax3.set_ylim(0, np.max(validation_loss))
    ax3.set_xlim(0, max(10, epoch_idx))
    np.max(np.append(training_loss, validation_loss))

    annotation_text = "\n".join([label_train, label_val])

    ax3.text(.9, .9, label_train, color="white", horizontalalignment='right', transform=ax3.transAxes)
    ax3.text(.9, .84, label_val, color="white", horizontalalignment='right', transform=ax3.transAxes)

    print(label_train)
    print(label_val)

    #ax3.legend([line_train, line_val], [label_train, label_val])

    dark_blue_gray = "#2c2b30"
    ax3.set_facecolor(dark_blue_gray)

    return fig