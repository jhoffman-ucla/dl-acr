import sys
import os
import random

import torch
from torch.utils.data import Dataset

from torchvision import transforms

import numpy as np
from phantom import RandomPhantom, ExactPhantom

import matplotlib.pyplot as plt

import pydicom

def RescaleToInterval(array):
    # I think we want to compress [-1000, 1000] -> [-0.5, 0.5]
    # Rescale input array to [0, 1]
    in_min = -1000
    in_max = 1000

    array = (array - in_min)/(in_max - in_min)

    out_min = 0.0
    out_max = 1.0

    return (out_max - out_min) * array + out_min

class AddNoiseHU(object):
    max_sd_in_hu = 10.0
    
    def __init__(self, max_sd_in_hu=10.0):
        self.max_sd_in_hu = max_sd_in_hu
    
    def __call__(self, sample):
        curr_sd = self.max_sd_in_hu*random.random()
        image = sample['image'] + curr_sd * np.random.randn(*(sample['image'].shape)).astype(np.float32)
        sample['image'] = image
        return sample
    
class Rescale(object):
    def __call__(self, sample):
        image, feature = sample['image'], sample['feature_vec']

        result = {
            'image': RescaleToInterval(image),
            'feature_vec': feature
        }
        return result

class ToTensor(object):
    def __call__(self, sample):
        image, feature = sample['image'], sample['feature_vec']
        return {'image': torch.from_numpy(image, ),
                'feature_vec': torch.from_numpy(np.asarray(feature, dtype=np.single))}

class ToTensor3Channel(object):
    def __call__(self, sample):
        image, feature = sample['image'], sample['feature_vec']
        # image = torchvision.transforms.Grayscale(num_output_channels=3)

        img =  torch.from_numpy(image, )

        return {'image': img.expand(3,*img.shape[1:]),
                'feature_vec': torch.from_numpy(np.asarray(feature, dtype=np.single))}

class ACRDataset(Dataset):
    def __init__(self, N, transform=None):
        self.N = N
        self.transform = transform
        #print("Using ACRPhantom dataset")
        #print("Note: this is an abstract 'loader' that is really more of a 'generator'")
        #print("      As currently configured, all data and feature vectors are generated on the fly")

    def __len__(self):
        return self.N

    def __getitem__(self, idx):
        array = None
        feature = None

        #if torch.is_tensor(idx):
        #    idx = idx.tolist()

        if type(idx) is not int:
            feat = idx
            array = ExactPhantom(*feat)
        else:
            array, feature = RandomPhantom()

        sample = {'image': array, 'feature_vec': feature}

        if self.transform:
            sample = self.transform(sample)

        return sample

# NOTE: in order to use this, NO other files can be present in the
# directory containing the image data.
class TestACRDataset(Dataset):

    dirpath = "./"
    dirlist = []

    def __init__(self, dirpath, transform = None):
        self.transform = transform
        self.dirpath = dirpath
        self.dirlist = os.listdir(dirpath)
        self.dirlist.sort()
        # print(self.dirlist)

    def __len__(self):
        return len(self.dirlist)

    def __getitem__(self, idx):
        fullpath = os.path.join(self.dirpath, self.dirlist[idx])
        ds = pydicom.dcmread(fullpath)
        img = ds.pixel_array

        # This is definitely throwing away data, but just to get something running:
        # Downsample from 512,512 to 1,256,256
        img = img[::2, ::2]
        img = ds.RescaleSlope*img + ds.RescaleIntercept
        img = np.expand_dims(img.astype(np.single), axis=0)

        sample = {'image': img, 'feature_vec': [0, 0, ds.ReconstructionDiameter, 0, False], 'path': fullpath}

        if self.transform:
            sample = self.transform(sample)

        return sample


if __name__=="__main__":

    tforms = transforms.Compose([Rescale(), ToTensor()])

    a = ACRDataset(100, transform=tforms)
    sample1 = a[1000]
    a = TestACRDataset("./data/dl-acr-test/", transform = tforms)
    sample2 = a[25]
    plt.subplot(1,2,1)
    plt.imshow(np.squeeze(sample1['image']), cmap='gray', vmin=0, vmax=1)
    plt.subplot(1,2,2)
    plt.imshow(np.squeeze(sample2['image']), cmap='gray', vmin=0, vmax=1)
    plt.show()
