import os
import sys
import torch
import numpy as np
import pydicom
import matplotlib.pyplot as plt

import nibabel as nib

import time
import json

from phantom import ACRPhantom, ExactPhantom, RenderBuffer
from dataset import RescaleToInterval
from model_def import ResNetEncoder

def usage():
    print("python inference_min.py INPUT.DCM OUTPUT_DIR WEIGHTS_FILE")

def infer(img, model):

    # Apply transforms to image
    img = img[::2, ::2]
    img = RescaleToInterval(img)
    img = np.expand_dims(img, axis = 0)
    img = np.expand_dims(img, axis = 0)
    img = img.astype(np.single)
    
    with torch.no_grad():
        res = model(torch.Tensor(img))
        
    res = res.detach().cpu().numpy()
    res = np.squeeze(res)
    return res

def AnalyzePhantom(img, res):
    network_output = {
        "x": res[0],
        "y": res[1],
        "fov": res[2],
        "rot": res[3],
        "flip": res[4],
        }

    # Renderbuffer is used to capture a nifti mask of the
    # rois used for analysis. Thus, we clear to 0.
    r = RenderBuffer(fov = network_output["fov"], dim = 512)
    r.Clear(clear_val = 0)
    
    ph = ACRPhantom()
    ph.SetPosition(network_output["x"], network_output["y"])
    ph.SetRotation(network_output["rot"])

    data = ph.Analyze(img, r)
    
    return data, r.buff

def SaveNiftiMask(mask, filepath, dicom=None):
    affine = np.eye(4)
    header = nib.nifti1.Nifti1Header()
    test = nib.load("dl-acr_AbdomenSeq_20220802114901_9.nii")
    header = test.header
    
    if dicom is not None:
        pass
        #image_position_patient = dicom.ImagePositionPatient
        #image_orientation_patient = dicom.ImageOrientationPatient
        #pixel_spacing = dicom.PixelSpacing
        #row_cosines = np.array(image_orientation_patient[:3])
        #column_cosines = np.array(image_orientation_patient[3:])
        #slice_cosines = np.cross(row_cosines, column_cosines)
        #qfac = -1
        #affine = np.array([
        #    [row_cosines[0] * pixel_spacing[0], column_cosines[0] * pixel_spacing[1], slice_cosines[0], -image_position_patient[0]],
        #    [row_cosines[1] * pixel_spacing[0], column_cosines[1] * pixel_spacing[1], slice_cosines[1], -image_position_patient[1]],
        #    [row_cosines[2] * pixel_spacing[0], column_cosines[2] * pixel_spacing[1], slice_cosines[2], dicom.SliceLocation],
        #    [0, 0, 0, 1]
        #])
        #header.set_qform(affine, code=1)

        
        #voxel_spacing = [dicom.PixelSpacing[0], dicom.PixelSpacing[1], dicom.SliceThickness, 1]
        #OM = np.eye(OM)
        #4 = OM * np.diag(voxel_spacing)
        #
        #affine = np.diag(list(dicom.PixelSpacing) + [dicom.SliceThickness, 1])
        #
        #q_form = np.diag([-1, -1, 1, 1])
        #q_form[:3, 3] = dicom.ImagePositionPatient
        #if hasattr(dicom, 'ImageOrientationPatient'):
        #    print(dicom.ImageOrientationPatient)
        #    q_form[:3, :3] = np.reshape(dicom.ImageOrientationPatient, (2, 3)).T
        #else:
        #    q_form[:3, :3] = np.identity(3)
        #    
        #s_form = np.zeros((4, 4))
        #s_form[0, 0] = dicom.PixelSpacing[0]
        #s_form[1, 1] = dicom.PixelSpacing[1]
        #s_form[2, 2] = dicom.SliceThickness
        #s_form[3, 3] = 1
        #
        #transform = np.dot(q_form, s_form)
        #
        ## Incorporate translations, offsets, and transforms into NIfTI header
        #header.set_qform(transform, code=1)
        #header.set_sform(transform, code=1)
        #header['xyzt_units'] = 2
        
    print(header.get_qform())
    nii_img = nib.Nifti1Image(mask, affine = affine, header = header)
    nib.save(nii_img, filepath)

    

def main(argc, argv):
    
    if argc < 4:
        print("Error: Not enough arguments")
        usage()
        sys.exit(1)

    if argc > 4:
        print("Error: Too many arguments")
        usage()
        sys.exit(1)

    input_img = argv[1]
    output_dir = argv[2]
    weights_file = argv[3]

    # handle input file
    ds = pydicom.dcmread(input_img)
    img = ds.RescaleSlope*ds.pixel_array + ds.RescaleIntercept
    
    # load model and execute
    start = time.time()
    m = torch.load(weights_file, map_location=torch.device('cpu'))
    m.eval()
    end = time.time()
    print("Time to load model: {}".format(end - start))

    # Run inference
    start = time.time()
    res = infer(img, m)
    end = time.time()
    print("Time to infer: {}".format(end - start))
    print("Network output: ", res)

    # Run analysis based off of network results
    d, analysis_mask = AnalyzePhantom(img, res)
    print("Automated analysis results: ")
    print(json.dumps(d, indent=2))

    # Create output directory
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    # Save mask to nifti file (not currently aligned correctly)
    mask_path = os.path.join(output_dir, "mask.nii")
    print("saving nifti mask to: ", mask_path)
    SaveNiftiMask(analysis_mask, mask_path, dicom = ds)

    # Save analysis results to json file
    json_path = os.path.join(output_dir, "result.json")
    print("saving results to: ", json_path)
    with open(json_path, 'w') as f:
        json.dump(d, f, indent=2)

    # show overlay
    plt.imshow(img, cmap= "gray", vmin =-100, vmax= 100)
    plt.imshow(analysis_mask, cmap = "jet", vmin = 0, vmax = 5, alpha = 0.5)
    plt.show()
    
if __name__=="__main__":
    main(len(sys.argv), sys.argv)