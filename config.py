import sys
import os
import yaml
import argparse

class Config():
    config_path = None
    options = {}
    parser = None

    def __init__(self, exe_name):
        self.parser = argparse.ArgumentParser(prog=exe_name)
        self.parser.add_argument("config_path", help="path to YAML config file. generate a new config with `./train.py new`")

    def AddOption(self, name, default, var_type):
        self.options[name] = default
        self.parser.add_argument("--"+name, type=var_type)

    def Save(self, filepath):
        with open(filepath, 'w') as f:
            yaml.dump(self.options, f)

    def GetYAML(self):
        return yaml.dump(self.options)

    def Load(self, filepath):
        with open(filepath, 'rb') as f:
            opts = yaml.load(f, Loader=yaml.Loader)
            # Merge parsed config file into options rather than
            # replace. This should promote the addition of new config
            # items without breaking backwards compatilibility with
            # older files.
            for k, v in opts.items():
                self.options[k] = v

    def __str__(self):
        return yaml.dump(self.options)

    def __getitem__(self, name):
        if name in self.options.keys():
            return self.options[name]
        else:
            return None

    def ParseArgs(self):
        args = self.parser.parse_args()
        for arg in vars(args):
            name = arg
            val = getattr(args, arg)
            if val is not None:
                self.options[name] = val

        if args.config_path == "new":
            print("Saving new config file to 'config.yaml'")
            self.Save("config.yaml")
            sys.exit(1)
        else:
            self.config_path = args.config_path

    def Get(self, config_option):
        return self.options[config_option]

    # Not generally recommmended, but useful when you need to inject
    # info back into a config (i.e. random seed values)
    def Set(self, key, value):
        self.options[key] = value

    def GetConfigPath(self):
        return self.config_path

if __name__=="__main__":

    # Here's an example of how you might use this class
    c = Config()

    if os.path.exists("test_config.yaml"):
        c.Load("test_config.yaml")

    c.AddOption("batch_size", 32, int)
    c.AddOption("learning_rate", 0.001, float)
    c.AddOption("model_file", "./my_model.pt", str)
    c.AddOption("output", "./output/", str)

    c.Save("test_config.yaml")
    c.ParseArgs()

    learning_rate = c.Get("learning_rate")
    print(learning_rate, type(learning_rate))

    batch_size = c["batch_size"]
    print(batch_size, type(batch_size))
