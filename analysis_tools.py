import numpy as np

def CircleROI(x, y, r, render_buff):
    X,Y = render_buff.GetPositionsArray()
    X_off = X-x;
    Y_off = Y-y;
    return np.multiply(X_off,X_off) + np.multiply(Y_off,Y_off) < r*r
    