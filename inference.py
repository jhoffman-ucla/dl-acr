import sys
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '2'

import torchvision.datasets as datasets
import torchvision.transforms as transforms

import torch
from torch import nn
from torch.utils.data import DataLoader
from dataset import TestACRDataset, ToTensor, ToTensor3Channel, Rescale
from phantom import ExactPhantom
from model_def import Encoder, ResNetEncoder

import numpy as np
from matplotlib import pyplot as plt

device = "cuda"


def get_run_index():
    files = [f for f in os.listdir("./output") if (os.path.isfile(f) and ("inference_sample" in f))]
    return len(files)

def plot_io(acr_input, acr_sim, save_path):
    # plot
    fig = plt.figure()
    ax1 = plt.subplot(1,2,1)
    ax1.set_title("Input ACR Phantom")
    ax1.imshow(np.squeeze(acr_input), cmap='gray', vmin=0, vmax=1)
    plt.axis('off')

    ax2 = plt.subplot(1,2,2)
    ax2.set_title("CNN Simulated ACR Phantom")
    ax2.imshow(np.squeeze(acr_sim), cmap='gray')
    plt.axis('off')
    plt.savefig(save_path, bbox_inches='tight')
    return


def eval_single_image(image, model):
    
    with torch.no_grad():
        pred = model(image.unsqueeze(0).to(device))

    # Do something with pred
    pred = pred.detach().cpu().numpy() # remove from computational graph to cpu and as numpy
    print(pred)
    return pred
    

def eval_dataset(data_test, model, case_dict):

    for key, value in case_dict.items():
        n = value['n']
        y = value['ref_feat']

        image = data_test[n]['image']
        pred = eval_single_image(image, model)

        acr_sim = ExactPhantom(*pred[0])

        save_path = os.path.join(outdir, f"inference_{key}.png")
        plot_io(image, acr_sim, save_path)

    return





def main(model_path, acr_test_dir, case_dict):

    model_name = os.path.splitext(os.path.basename(model_path))[0]
    outdir = os.path.join('./output', model_name)
    if not os.path.exists(outdir):
        os.makedirs(outdir)


    model = torch.load(model_path)
    model.eval()

    tforms = transforms.Compose([Rescale(), ToTensor()])
    # tforms = transforms.Compose([Rescale(), ToTensor3Channel()])
    data_test = TestACRDataset(acr_test_dir, transform = tforms)

    # kwargs = {"num_workers": 1, "pin_memory": True} if (device == "cuda") else {}
    # batch_size = 32
    # test_dataloader = DataLoader(data_test, batch_size = batch_size, shuffle = True, **kwargs)
    # print(data_test.shape)
    # print(model)

    # in progress, not implemented correctly yet
    # eval_dataset(data_test, model, case_dict)

    n = 5
    image = data_test[n]['image']
    # print(sample)

    pred = eval_single_image(image, model)
   
    # apply transformation
    acr_sim = ExactPhantom(*pred[0])

    save_path = os.path.join(outdir, f"inference_sample_{n}.png")
    plot_io(image, acr_sim, save_path)

    


# Call our main function
if __name__=="__main__":

    acr_test_dir = './data/dl-acr-test'
    model_path = './baseline_resnet50.pt'
    test_index = 5
    
    
    main(model_path, acr_test_dir, test_index)
