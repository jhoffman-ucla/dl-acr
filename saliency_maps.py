import sys
import os
import argparse
import time
import ast
from copy import deepcopy

import torch
from torchvision import transforms
import matplotlib.pyplot as plt
import numpy as np
import pydicom

from phantom import RandomPhantom, ExactPhantom
from dataset import ACRDataset, TestACRDataset, Rescale, ToTensor, RescaleToInterval

#device = "cuda"

def jitter(feat):
    feat = np.array(feat)
    return feat + 10.0*(np.random.rand(feat.shape[0], feat.shape[1]) - 0.5)

# image should be a 2d numpy array
def SaliencyMap(model, image, feat, device):

    loss_fn = torch.nn.MSELoss()

    image_T = torch.Tensor(image)
    image_T = image_T.to(device)
    image_T.requires_grad_()
    image_T.retain_grad()

    V = model(image_T)

    # Do backpropagation to get the derivative of the output based on the image

    if feat is None:
        tmp_feat = torch.clone(V)
        feat = jitter(tmp_feat.detach().cpu().numpy())

    feat_T = torch.reshape(torch.Tensor(feat), [1,5])
    feat_T = feat_T.to(device)
    loss = loss_fn(V, feat_T)

    #loss.backward(retain_graph = True)
    loss.backward()

    saliency, _ = torch.max(image_T.grad.data.abs(), dim=1)
    saliency = saliency.reshape(256, 256)
    saliency = saliency.detach().cpu().numpy()

    return saliency

def main():

    # CLI
    parser = argparse.ArgumentParser(description="Generate saliency maps for dl-acr data (random, exact, or from test set)")
    parser.add_argument("model_file")
    parser.add_argument("--output", help="save map to file (instead of display)")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--dicom", help="path to DICOM file")
    group.add_argument("--feature", help="feature vector of the form [pos_x, pos_y, fov, rotation_angle, flip]")
    group.add_argument("--testimage", help="one of: normal rotated shifted_rotated med_fov lrg_fov")
    args = parser.parse_args()

    # Load our image
    tforms = transforms.Compose([Rescale(), ToTensor()])
    ds = TestACRDataset("/home/john/Data/dl-acr-test", transform=tforms)
    acr_ds = ACRDataset(1000, transform=tforms)

    ######################
    # Image loading code
    ######################
    third_arg = None
    # Load from sample scans of ACR
    if args.testimage is not None:
        third_arg = args.testimage

        acr_normal = ds[203]
        acr_rotated = ds[168]
        acr_shifted_rotated = ds[200]
        acr_med_fov = ds[0]
        acr_lrg_fov = ds[1]

        feat_normal          = [1         , 2        , 252    , 0          , 0.]
        feat_rotated         = [7         , 2        , 252    , -47.3563   , 0.]
        feat_shifted_rotated = [6.0891294 , 42.634827, 250.7  , -45        , 0.]
        feat_med_fov         = [7.5518885 , 3        , 305    , -1.4070848 , 0.]
        feat_lrg_fov         = [6         , 0        , 507    , 0          , 0.]

        #image = np.reshape(acr_shifted_rotated['image'], [1, 1, 256,256])
        image = eval("np.reshape(acr_%s['image'], [1, 1, 256,256])" % args.angle_or_sample)
        feat = eval("feat_%s" % args.angle_or_sample)

    # Load an DICOM from any file
    elif args.dicom is not None:
        ds = pydicom.dcmread(args.dicom)
        image = RescaleToInterval(ds.pixel_array)
        image = image[::2, ::2]
        image = np.reshape(image, [1,1,256,256])
        feat = None
        pass

    # Load an exact phantom

    elif args.feature is not None:
        third_arg = args.feature
        feat = ast.literal_eval(args.feature)

        #feat = (0, 0, 350, i-180, 0)
        acr_exact = acr_ds[feat]
        image = acr_exact['image']
        image = np.reshape(image, [1, 1, 256, 256])

    # Generate random phantom (w/ proper preprocessing)
    else:
        third_arg = "random"
        acr_sample = acr_ds[0]
        image = np.reshape(acr_sample['image'], [1, 1, 256,256])
        feat = acr_sample['feature_vec']

    # Load model
    model = torch.load(args.model_file, map_location=device)

    # Run saliency
    if feat==None:
        # feat == None when loading a DICOM image. For most images, we
        # don't have an known good feature to compare against.  To
        # prevent the loss calculation from being zero (by using the
        # exact network output), we "jitter" the network output to
        # induce a non-zero gradient.  Because the jitter is random
        # though and does have an impact, we sample N different
        # jittered vectors and take the mean saliency map as our final
        # saliency map.  **I have no idea if this is a valid
        # approach**, but it does at least result in a
        # semi-reproducible output and seems to be somewhat
        # "scale-invariant" to the size of the jitter (which "feels"
        # like a good property)
        N = 50
        gather = np.zeros((N, 256, 256))
        for i in range(N):
            print("jitteration ", i)
            saliency = SaliencyMap(model, image, feat)
            gather[i,:,:] = saliency

        saliency = np.mean(gather,axis=0)
    else:
        saliency = SaliencyMap(model, image, feat)

    f = GenerateFigure(image, saliency)

    # Plot it
    if args.output is not None:
        plt.savefig("%s" % args.output)
    else:
        plt.show()

if __name__=="__main__":
    main()
