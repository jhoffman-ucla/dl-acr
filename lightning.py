import os
from torch import optim, nn, utils, Tensor
from torch.utils.data  import DataLoader

from torchvision.datasets import MNIST
from torchvision.transforms import ToTensor
import torchvision.transforms as transforms


import pytorch_lightning as pl
print(pl.__version__)

from config import Config
from dataset import ACRDataset, ToTensor, Rescale
from model_def import Encoder, ResNetEncoder


# define the LightningModule
class PhantomEncoder(pl.LightningModule):
    def __init__(self, encoder):
        super().__init__()
        self.encoder = encoder

    def training_step(self, batch):
        # training_step defines the train loop.
        # it is independent of forward
        X, y = batch["image"], batch["feature_vec"]
        device = 'auto'
        # X = X.to(device)
        # y = y.to(device)

        # compute prediction and loss
        pred_ph = self.encoder(X)
        # pred_ph = pred_ph.to(device)

        loss = nn.functional.mse_loss(pred_ph, y)

        # Logging to TensorBoard (if installed) by default
        self.log("train_loss", loss)
        return loss

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=1e-3)
        return optimizer



def main():

    conf = Config()
    conf.AddOption("batch_size", 32, int)
    conf.AddOption("learning_rate", .001, float)
    conf.AddOption("device", "cpu", str)
    conf.AddOption("epochs", 100, int)
    conf.AddOption("loss_fn", "nn.MSELoss()", str) # This may be a bad idea, but oh well (using eval to set from config)
    conf.AddOption("model_arch", "ResNetEncoder()", str) # This may be a bad idea, but oh well (using eval to set from config)
    conf.AddOption("output_dir", "./", str)
    conf.AddOption("training_set_size", 5000, int)
    conf.AddOption("validation_set_size", 200, int)
    
    # Clumsy but it works
    conf.ParseArgs() # Primes the config_path property
    conf.Load(conf.GetConfigPath())
    conf.ParseArgs() # Call a 2nd time to override with command line params

    device = conf["device"]

    # Hyperparameters
    learning_rate = conf["learning_rate"]
    batch_size = conf["batch_size"]
    epochs = conf["epochs"]

    # init the encoder
    encoder = PhantomEncoder(Encoder())

    # Instatiate data loaders
    tforms = transforms.Compose([Rescale(),ToTensor()])
    data_train = ACRDataset(conf["training_set_size"], transform=tforms)
    # data_test = ACRDataset(conf["validation_set_size"], transform=tforms)

    # setup data
    kwargs = {"num_workers": 1, "pin_memory": True} if (device == "cuda") else {}
    train_loader = DataLoader(data_train, batch_size = 1, shuffle = True, **kwargs)
    # test_loader = DataLoader(data_test, batch_size = batch_size, shuffle = True, **kwargs)

    trainer = pl.Trainer(limit_train_batches=100, max_epochs=5, log_every_n_steps=10,  accelerator='auto')
    trainer.fit(model=encoder, train_dataloaders=train_loader)


if __name__=="__main__":
    main()