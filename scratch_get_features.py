from phantom import ExactPhantom
from dataset import TestACRDataset
import matplotlib.pyplot as plt
import numpy as np

import torch
from torch import nn

ds = TestACRDataset("./data/dl-acr-test/")

#acr_normal
#[[2.0268796e-01 5.0497255e+00 2.4216518e+02 1.3722569e+00 0.0000000e+00]]
#
#acr_rotated
#[[  6.056059    6.4476995 244.10521   -47.3563      0.       ]]
#
#acr_shifted_rotated
#[[  7.0891294  42.634827  247.4637    -44.33322     0.       ]]
#
#acr_med_fov
#[[  4.5518885   6.6508646 289.87878    -1.4070848   0.       ]]
#
#acr_lrg_fov
#[[ -3.1154299  29.233908  396.16574    -2.2222204   0.       ]]

# Summary figure with raw network outputs
def GenerateNetworkOutputSummary():
    acr_normal = ds[203]
    acr_rotated = ds[168]
    acr_shifted_rotated = ds[200]
    acr_med_fov = ds[0]
    acr_lrg_fov = ds[1]
    
    plt.subplot(1,5,1)
    feat_normal = [2.0268796e-01, 5.0497255e+00, 2.4216518e+02, 1.3722569e+00, 0.0000000e+00]
    plt.imshow(np.squeeze(acr_normal['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_normal)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.25);
    
    plt.subplot(1,5,2)
    feat_rotated = [  6.056059,    6.4476995, 244.10521,   -47.3563,      0.       ]
    plt.imshow(np.squeeze(acr_rotated['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_rotated)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.25);
    
    feat_shifted_rotated = [  7.0891294,  42.634827,  247.4637,    -44.33322,     0.       ]
    plt.subplot(1,5,3)
    plt.imshow(np.squeeze(acr_shifted_rotated['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_shifted_rotated)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.25);
    
    feat_med_fov = [  4.5518885,   6.6508646, 289.87878,    -1.4070848,   0.       ]
    plt.subplot(1,5,4)
    plt.imshow(np.squeeze(acr_med_fov['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_med_fov)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.25);
    
    feat_lrg_fov = [ -3.1154299,  29.233908,  396.16574,    -2.2222204,   0.       ]
    
    plt.subplot(1,5,5)
    plt.imshow(np.squeeze(acr_lrg_fov['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_lrg_fov)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.25);
    
    plt.savefig("./output/fig.png")

    plt.show()


# Summary figure with raw network outputs
def GenerateIdealFeatureSummary():
    acr_normal = ds[203]
    acr_rotated = ds[168]
    acr_shifted_rotated = ds[200]
    acr_med_fov = ds[0]
    acr_lrg_fov = ds[1]
    
    plt.subplot(1,5,1)
    feat_normal = [1, 2, 252, 0, 0.0000000e+00]
    plt.imshow(np.squeeze(acr_normal['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_normal)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.75);
    
    plt.subplot(1,5,2)
    feat_rotated = [ 7,    2, 252,   -47.3563,      0.]
    plt.imshow(np.squeeze(acr_rotated['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_rotated)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.75);
    
    feat_shifted_rotated = [  6.0891294,  42.634827,  250.7,    -45,     0.       ]
    plt.subplot(1,5,3)
    plt.imshow(np.squeeze(acr_shifted_rotated['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_shifted_rotated)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.75);
    
    feat_med_fov = [  7.5518885,   3, 305,    -1.4070848,   0.       ]
    plt.subplot(1,5,4)
    plt.imshow(np.squeeze(acr_med_fov['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_med_fov)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.75);
    
    feat_lrg_fov = [ 6,  0, 507,    0,   0.]
    
    plt.subplot(1,5,5)
    plt.imshow(np.squeeze(acr_lrg_fov['image']) ,cmap='gray', vmin=-100, vmax=100)
    ph = ExactPhantom(*feat_lrg_fov)
    plt.imshow(np.squeeze(ph), cmap="jet", alpha = 0.75);
    
    plt.show()


GenerateIdealFeatureSummary()
GenerateNetworkOutputSummary()

# Target feature vectors for Morgan:
#feat_normal          = [1         , 2        , 252    , 0          , 0.]
#feat_rotated         = [7         , 2        , 252    , -47.3563   , 0.]
#feat_shifted_rotated = [6.0891294 , 42.634827, 250.7  , -45        , 0.]
#feat_med_fov         = [7.5518885 , 3        , 305    , -1.4070848 , 0.]
#feat_lrg_fov         = [6         , 0        , 507    , 0          , 0.]