import sys
import os

import torch
from torch import nn

from phantom import ExactPhantom
from dataset import RescaleToInterval
import matplotlib.pyplot as plt

import numpy as np

# Want to generate a bunch of phantoms and observe how loss changes
# Key takeaways:
#   - Loss has good variation for position and FOV
#   - Loss has "dead zones" for rotation where changes in the rotation
#     parameter do not really cause changes in the loss. This could prevent
#     training.

loss_fn = nn.MSELoss()

acr_ref = ExactPhantom(0,0,250,0,False)
acr_ref = RescaleToInterval(acr_ref)

N = 30

for i in range(N):

    var = 3*i + 200
    arglist = [0, 0, var, 0, False]
    
    acr = ExactPhantom(*arglist)
    
    plot_idx = 2*i
    
    plt.subplot(N,2,plot_idx + 1 )
    plt.imshow(np.squeeze(acr_ref))
    plt.subplot(N,2,plot_idx + 2 )
    plt.imshow(np.squeeze(acr))
    loss = loss_fn(torch.Tensor(acr_ref), torch.Tensor(RescaleToInterval(acr)))
    print(loss)
    plt.title(str(loss))
    
plt.show()