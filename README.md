# DL-ACR

Ideally, self-training registration/detection for phantoms (~~not~~ working better than expected).

## Prereqs

You can see a complete list of packages in `env/*.yaml`.  If you're using anaconda, see below for how to create an environment from one of those files.

Typically though, I usually start one of the following:

```bash
# Linux
conda install pytorch torchvision torchaudio cpuonly -c pytorch # OR
conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia # OR
conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia

# MacOS
conda install pytorch::pytorch torchvision torchaudio -c pytorch
```

The proceed to `conda install` anything python complains about.

## To run

Train:
```python
# Generate a new config file if one not already present
python train.py new

# Assuming you're using config.yaml (change to name of your config file)
python train.py config.yaml

# All config parameters can be overridden via the command line
python train.py config.yaml --output_dir output2/ --learning_rate 0.001

# For more info on overriddable parameters
python train.py --help

# You can summarize all results in an output_dir:
python summarize.py output2/
```

Inference:

```
python inference_min.py DICOM_FILE OUTPUT_DIR MODEL_FILE.pt
```

This will run the network on DICOM_FILE to produce a feature vector and ACR HU module analysis.  Results are output into `OUTPUT_DIR`, which is created if it does not exist.

Tensorboard:
```
tensorboard --logdir=tb_logs/ --port=6006
```

To create a conda environment from one of the environment files in `env`:
```
conda env create -f env/<environment>.yaml
```

### Tested Environments
#### Linux w/ Anaconda (John's Linux Desktop)
Ubuntu 22.04 w/ CUDA support on RTX-3060 - 12GB of VRAM

I haven't been especially careful, but MacOS advice probably applies: only use `conda install` to add packages.  Intermixing `pip` and `conda` hasn't been as fussy as is was on MacOS, but still probably a better idea to keep the environment consistent.

#### MacOS w/ Anaconda (John's Macbook Air)
MacOS Ventura 13.3.1 (a) with MPS support enabled

Lessons learned:
1. Only use Conda to install packages.  May be obvious to everyone else, but I broke my PyTorch install multiple times by accidentally intermixing `pip install`s with `conda install`s.  Sticking to the pure conda seems to produce a state/environment that has non-buggy MPS (apple's GPU pytorch device type) support.  Installed anaconda using the [GUI installer](https://www.anaconda.com/download#macos).

## Other stuff
- Config defaults should be pretty reasonable for most use cases.
- Likely to be easiest to change training hyperparameters using CLI overrides.  Runtime configuration values are saved to the model\_output\_dir
- By default, auto-detects accelerators `cuda` and `mps`, falls back to `cpu`
- Set specific device via config file or `./train.py config.yaml --device cuda:0`
- Any file starting with `scratch_` is not really a part of the main project. Should probably be removed at some point in the future.
