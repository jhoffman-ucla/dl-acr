import sys
import os

import torch
from torch import nn

from phantom import ExactPhantom
#from torchsummary import summary
from torchvision import models

from matplotlib import pyplot as plt
from dataset import RescaleToInterval

import math
import numpy as np

def clamp(num, min_value, max_value):
   return max(min(num, max_value), min_value)

class MultiplicativeLayer(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        pos_min, pos_max = (-50, 50)
        fov_min, fov_max = (200, 400)
        rot_min, rot_max = (0, 359)

        # Only supports our Nx1x5 feature vectors for ACR phantom
        #       (pos_x, pos_y, fov, rot, flip)
        for i in range(x.size()[0]):
            curr_feat = x[i, :]
            curr_feat[0] = curr_feat[0]
            curr_feat[1] = curr_feat[1]
            curr_feat[2] = curr_feat[2]
            curr_feat[3] = curr_feat[3]
            # curr_feat[3] = 0
            curr_feat[4] = False

            x[i, :] = curr_feat

        return x

class Decoder(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        N = x.size()[0]
        final = np.zeros((N, 1, 256, 256), dtype=np.single)
        # x should be a tensor of feature vectors.  For each vector, we need to translate back to
        # to an image, and store in a [N, 1, 256, 256] tensor.
        for i in range(N):
            curr_feat = x[i]
            #pos_x, pos_y, fov, rot, flip = curr_feat.detach().numpy()
            pos_x, pos_y, fov, rot, flip = curr_feat.cpu().detach().numpy()

            # could clamp above values to our min/max if we have trouble training
            final[i,0,:,:] = RescaleToInterval(ExactPhantom(pos_x, pos_y, fov, rot, bool(flip)))
        
        ten = torch.Tensor(final)
        ten.requires_grad_()

        return ten

class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()

        self.encode = nn.Sequential(
           nn.Conv2d(1, 4, 3, stride=1, padding=1 ), # Out:256x256x4
           nn.MaxPool2d(2, stride=2), # Out 128x128x4
           nn.ReLU(),

           nn.Conv2d(4, 8, 3, stride=1, padding=1), # Out 128x128x8
           nn.MaxPool2d(2, stride=2), # 64x64x8
           nn.ReLU(),

           nn.Conv2d(8, 16, 3, stride=1, padding=1), # Out 64x64x16 (or 32x32x32?)
           nn.MaxPool2d(2, stride=2), # 64x64x16
           nn.ReLU(),

           nn.Conv2d(16, 32, 3, stride=1, padding=1), # Out 64x64x32 
           nn.MaxPool2d(2, stride=2), # 32x32x32
           nn.ReLU(),

           nn.MaxPool2d(2, stride=2),
           nn.ReLU(),
           
           nn.Flatten(1),
           nn.Linear(2048, 2048),
           nn.ReLU(),
           
           nn.Linear(2048, 100),
           nn.ReLU(),

           nn.Linear(100, 5),
           MultiplicativeLayer(),
        )

        # self.decode = Decoder()

        #test = self.encode(torch.Tensor(np.random.rand(2,1,256,256)))
        #print(test.size())
        #
        #res = self.decode(test)
        #print(res.size())
        #
        # summary(self, (1, 256, 256))

    def forward(self, x):

        feat = self.encode(x.float())

        # Need to convert our encoded vectors back into image tensors
        # return self.decode(feat)

        # M: trying to just train on features
        return feat


class ResNetEncoder(nn.Module):
    def __init__(self):
        super(ResNetEncoder, self).__init__()

        # pull resnet50 arch from pytorch
        resnet50 = models.resnet50(weights=None, num_classes=5)
        num_ftrs = resnet50.fc.in_features
        # change input channels from 3 to 1
        resnet50.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        # change fully connected layer to linear
        resnet50.fc = nn.Linear(num_ftrs, 5)

        self.encode = nn.Sequential(
           resnet50,
           MultiplicativeLayer(),
        )

        # self.decode = Decoder()

        #test = self.encode(torch.Tensor(np.random.rand(2,1,256,256)))
        #print(test.size())
        #
        #res = self.decode(test)
        #print(res.size())
        #
        # summary(self, (1, 256, 256))

    def forward(self, x):

        feat = self.encode(x.float())

        # Need to convert our encoded vectors back into image tensors
        # return self.decode(feat)

        # M: trying to just train on features
        return feat