#!/bin/env python3
import sys
import os
import glob
import random

import numpy as np
from matplotlib import pyplot as plt

import torchvision.datasets as datasets
import torchvision.transforms as transforms

import torch
from torch import nn
from torch.utils.data  import DataLoader
from torch.utils.tensorboard import SummaryWriter

from config import Config
from dataset import ACRDataset, ToTensor, ToTensor3Channel, Rescale, RescaleToInterval, AddNoiseHU
from model_def import Encoder, ResNetEncoder
from saliency_maps import SaliencyMap

from training_history import TrainingHistory

import plotting

# TODO: more informative model naming? (I like to name with epoch and loss)
def get_save_path():
    return "model_{:02d}.pt".format(get_run_index())

def get_run_index():
    files = [f for f in os.listdir(".") if (os.path.isfile(f) and (".pt" in f))]
    return len(files)

# Trying to clean up this naming flow a little bit
def GetModelDirs(output_dir):
    dirs = [d for d in os.listdir(output_dir) if (os.path.isdir(os.path.join(output_dir, d)) and ("model_" in d))]
    dirs.sort()
    return dirs

def GetModelIndex(output_dir):
    dirs = GetModelDirs(output_dir)
    return len(dirs)

def GetModelName(output_dir):
    return "model_%03d" % GetModelIndex(output_dir)

# TODO: this should move to "plotting.py"
def plot_loss(train_loss, val_loss, save_dir):

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    fig = plt.figure()
    plt.plot(train_loss, label='train_loss')
    plt.plot(val_loss, label='val_loss')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend()
    plt.savefig(os.path.join(save_dir, 'train_progress.png'))
    return fig

class RollingEarlyStoppingManager():
    def __init__(self, min_epochs=10, patience=3, tolerance = 0):
        self.min_epochs = min_epochs
        self.patience = patience
        self.tolerance = tolerance
        self.history = []
        self.counter = 0

    def record_and_check(self, validation_loss):

        self.history.append(validation_loss)

        trigger_early_stop = False

        # Don't stop if we haven't trained for the minimum number of epochs just yet
        if len(self.history) < self.min_epochs:
            return trigger_early_stop

        # Compute the rolling average change over
        interval = self.history[-self.patience:]
        rolling_change = np.mean(np.diff(interval))
        if rolling_change > self.tolerance:
            print(f"Early stopping: {rolling_change} > {self.tolerance} (stopping!)" )
            trigger_early_stop = True
        else:
            print(f"Early stopping: {rolling_change} <= {self.tolerance} (continuing)" )

        return trigger_early_stop

# M: changed the EarlyStoppingManager to have the behavior that I'd expect
#     (that is, stopping if no improvement after # epochs given by patience)
#      (the rolling average calc is interesting too, but was looking for this)
class EarlyStoppingManager():
    def __init__(self, min_epochs=10, patience=3, tolerance = 0):
        self.min_epochs = min_epochs
        self.patience = patience
        self.tolerance = tolerance
        self.history = []
        self.counter = 0
        self.min_validation_loss = np.inf


    def record_and_check(self, validation_loss):

        self.history.append(validation_loss)

        trigger_early_stop = False

        # Don't stop if we haven't trained for the minimum number of epochs just yet
        if len(self.history) < self.min_epochs:
            return trigger_early_stop

        if validation_loss < self.min_validation_loss:
            self.min_validation_loss = validation_loss
            self.counter = 0
        elif validation_loss > (self.min_validation_loss + self.tolerance):
            self.counter += 1
            if self.counter >= self.patience:
                trigger_early_stop = True
        return trigger_early_stop

def train_loop(dl, model, loss_fn, optimizer, device):
    size = len(dl.dataset)

    for i, batch in enumerate(dl):

        X, y = batch["image"], batch["feature_vec"]

        X = X.to(device)
        y = y.to(device)

        # compute prediction and loss
        pred_ph = model(X)
        pred_ph = pred_ph.to(device)

        # Open question: MSELoss on feature vector directly or between images?
        #    I believe the image approach is the correct one. Encountered bad
        #    gradient blowup on feature vector.
        #
        # M: trying to train on features
        # loss = loss_fn(pred_ph, X)
        loss = loss_fn(pred_ph, y)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if i % 100 == 0:
            loss, current = loss.item(), i * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

    # M: adding so I can plot loss
    return loss.item()

def test_loop(dl, model, loss_fn, device, epoch_idx=None):
    best_loss = 100000000
    best_feat = None
    actual_best = None

    size = len(dl.dataset)
    num_batches = len(dl)
    test_loss, correct = 0, 0

    # Removing torch.no_grad() since we DO want to call backwards to
    # generate the saliency map.  It's faster to disable it (dunno
    # actually, haven't measured, but according to the docs it is and
    # reduces memory consumption) but here we trade a bit of
    # performance for added functionality.
    #
    # with torch.no_grad():
    with open("/dev/null", "w") as r: # This is just a dummy statement so I don't have to re-indent the whole block below
        for batch in dl:

            X, y = batch["image"], batch["feature_vec"]

            X = X.to(device)
            y = y.to(device)

            pred = model(X)
            pred = pred.to(device)
            # M: trying to train on features
            # curr_loss = loss_fn(pred, X).item()
            curr_loss = loss_fn(pred, y).item()

            if curr_loss < best_loss:
                best_feat = model.encode(X)
                actual_best = y

            test_loss += curr_loss

    test_loss /= num_batches

    print(f"Test Error: \n     Avg loss: {test_loss:>8f} \n")
    print("actual_best - best_guess:")
    print(actual_best - best_feat)

    return test_loss

def main():

    # Configuration handling
    #
    # NOTE: Once an item is added, it should not be renamed or
    # removed. If users should not use it in the future, it should be
    # explicitly deprecated and inform users of either:
    #   - Alternatives that should be used instead
    #   - Lack of support (e.g., "train.py no longer supports configuration option blah" ).
    #
    # In general though, in the interest of reproducible science, I
    # would like us to preserve behavior and maximize backwards compatibility.
    # TODO: Add mechanism to Config to deprecate options w/ explicit reason.
    #
    #
    conf = Config(sys.argv[0])
    conf.AddOption("version", "v0.0.1", str)
    conf.AddOption("batch_size", 32, int)
    conf.AddOption("learning_rate", .001, float)
    conf.AddOption("device", "auto", str)
    conf.AddOption("epochs", 100, int)
    conf.AddOption("loss_fn", "nn.MSELoss()", str) # This may be a bad idea, but oh well (using eval to set from config)
    conf.AddOption("model_arch", "Encoder()", str) # This may be a bad idea, but oh well (using eval to set from config)
    conf.AddOption("output_dir", "./output", str)
    conf.AddOption("training_set_size", 1000, int)
    conf.AddOption("validation_set_size", 100, int)
    conf.AddOption("save_model_history", False, bool)
    conf.AddOption("generate_training_summ_gif", True, bool)
    conf.AddOption("train_add_noise", 0.0, float) # stddev in hu
    conf.AddOption("test_add_noise", 0.0, float) # stddev in hu
    conf.AddOption("rng_torch_seed", 0, int)
    conf.AddOption("rng_random_seed", 0, int)
    conf.AddOption("rng_numpy_seed", 0, int)

    # Clumsy but it works
    conf.ParseArgs() # Primes the config_path property
    conf.Load(conf.GetConfigPath())
    conf.ParseArgs() # Call a 2nd time to override with command line params

    print("========================================")
    print("DL-ACR Training Script")
    print("========================================")
    print("using config: {}".format(conf.GetConfigPath()))
    print("Launching with the following settings:")
    print("")
    print(conf.GetYAML())

    # RNG/reproducibility
    torch_seed = 0
    random_seed = 0
    numpy_seed = 0

    torch.backends.cudnn.benchmark = False
    torch.use_deterministic_algorithms(True)

    if conf["rng_torch_seed"] != 0:
        torch_seed = conf["rng_torch_seed"]
    else:
        torch_seed = random.randint(1,2**32)

    g = torch.Generator()
    g.manual_seed(torch_seed)
    torch.manual_seed(torch_seed)

    if conf["rng_random_seed"] != 0:
        random_seed = conf["rng_random_seed"]
    else:
        random_seed = random.randint(1,2**32)

    random.seed(random_seed)

    if conf["rng_numpy_seed"] != 0:
        numpy_seed = conf["rng_numpy_seed"]
    else:
        numpy_seed = random.randint(1, 2**32)

    np.random.seed()

    conf.Set("rng_torch_seed", torch_seed)
    conf.Set("rng_random_seed", random_seed)
    conf.Set("rng_numpy_seed", numpy_seed)

    # Configure key output paths:
    #
    # NOTE: All non-tensorboard/lightning output (figures, plots,
    # csvs, model files, etc.) should be directed into
    # model_output_dir.  If possible, prefer flat directory (i.e. **no
    # subdirectories below model_output_dir.**
    base_output_dir = conf["output_dir"]
    model_idx = GetModelIndex(base_output_dir)
    model_name = GetModelName(base_output_dir)
    model_output_dir = os.path.join(base_output_dir, model_name)

    print("----------------------------------------")
    print("Output info:")
    print("    base_output_dir:  ", base_output_dir)
    print("    model_name:       ", model_name)
    print("    model_output_dir: ", model_output_dir)
    print("----------------------------------------")

    # Ensure model output dir exists. Fail if we can't create it.
    if not os.path.isdir(model_output_dir):
        # Only capturing the error here because permissions are
        # sometimes an issue at CVIB and we'd rather fail early than
        # do a full run and save no output.
        try:
            os.makedirs(model_output_dir, exist_ok = True)

        # we don't get more precise than this (i.e., handling specific
        # exception types), because any exception will be considered a
        # fatal error.
        except Exception as e:
            print(e)
            print("FATAL ERROR: model_output_dir creation failed and does not already exist.")
            sys.exit(1)

    # Save current config into model output_dir
    conf.Save(os.path.join(model_output_dir, "config.yaml"))

    # Device configuration
    device = conf["device"]

    if device == "auto":
        if torch.cuda.is_available():
            device = "cuda"
        elif torch.backends.mps.is_available():
            device = "mps"
        else:
            device = "cpu"
    print("Configured device: {}".format(conf["device"]))
    print("Using device: ", device)

    # Instantiate Model
    active_model = eval(conf["model_arch"])
    active_model.to(device)

    # Instatiate data loaders
    if conf["train_add_noise"] != 0.0:
        print("adding noise to training set: ", conf["train_add_noise"])
        train_tforms = transforms.Compose([AddNoiseHU(max_sd_in_hu = conf["train_add_noise"]), Rescale(), ToTensor()])
    else:
        train_tforms = transforms.Compose([Rescale(), ToTensor()])

    if conf["test_add_noise"] != 0.0:
        print("adding noise to test set: ", conf["train_add_noise"])
        test_tforms = transforms.Compose([AddNoiseHU(max_sd_in_hu = conf["test_add_noise"]), Rescale(), ToTensor()])
    else:
        test_tforms = transforms.Compose([Rescale(), ToTensor()])

    data_train = ACRDataset(conf["training_set_size"], transform = train_tforms)
    data_test  = ACRDataset(conf["validation_set_size"], transform = test_tforms)

    kwargs = {"num_workers": 1, "pin_memory": True} if (device == "cuda") else {}

    # Hyperparameters
    learning_rate = conf["learning_rate"]
    batch_size = conf["batch_size"]
    epochs = conf["epochs"]
    print("----------------------------------------")
    print("Current hyperparameters")
    print("    learning_rate: {}".format(learning_rate))
    print("    batch_size:    {}".format(batch_size))
    print("    epochs:        {}".format(epochs))
    print("----------------------------------------")

    def seed_worker(worker_id):
        worker_seed = torch.initial_seed() % 2**32
        np.random.seed(worker_seed)
        random.seed(worker_seed)

    train_dataloader = DataLoader(data_train, batch_size = batch_size, shuffle = True, worker_init_fn=seed_worker, generator=g, **kwargs)
    test_dataloader = DataLoader(data_test, batch_size = batch_size, shuffle = True, worker_init_fn=seed_worker, generator=g, **kwargs)

    def count_parameters(model):
        return sum(p.numel() for p in model.parameters() if p.requires_grad)

    n_param = count_parameters(active_model)
    print("Model \"{}\" has {} tuneable parameters".format(conf["model_arch"], n_param))

    # Train
    loss_fn = eval(conf["loss_fn"])

    optimizer = torch.optim.Adam(active_model.parameters(), lr=learning_rate) # M: switched SGD for Adam
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=4, verbose=True)

    early_stopper = EarlyStoppingManager(min_epochs=epochs, patience=6, tolerance=0)

    best_epoch = 0
    best_loss = 100000000
    train_loss_list = []
    test_loss_list = []
    tb_writer = SummaryWriter('tb_logs')

    #model_save_path = get_save_path() # M: moving this out here because we need to overwrite
    #model_name = "model_{:02d}".format(get_run_index())
    #save_dir = os.path.join('.', 'output', model_name)

    th = TrainingHistory(epochs)
    for epoch_idx in range(epochs):
        print("========================================")
        print("Epoch {}".format(epoch_idx))
        print("========================================")
        train_loss = train_loop(train_dataloader, active_model, loss_fn, optimizer, device)
        test_loss = test_loop(test_dataloader, active_model, loss_fn, device, epoch_idx = epoch_idx)
        scheduler.step(test_loss)

        train_loss_list.append(train_loss)
        test_loss_list.append(test_loss)

        tb_writer.add_scalars(model_name, {'loss/train': train_loss,
                                        'loss/val': test_loss,}, epoch_idx)

        # Generate a saliency map for the phantom
        a = ACRDataset(1, transform=transforms.Compose([Rescale(), ToTensor()]))

        feat = [0, 0, 250 + 2*epoch_idx, 2*epoch_idx, 0]
        ph = a[feat]
        img_T = torch.Tensor(ph['image'])

        img_T = img_T.reshape([1,1,256,256])
        saliency = SaliencyMap(active_model, img_T, feat, device)
        th.Record(epoch_idx, test_loss, train_loss, saliency)

        f = plotting.PlotEpochSaliencyMapWithLoss(ph['image'], saliency, epoch_idx, th.GetTrainingLoss(), th.GetValidationLoss())
        f.savefig(os.path.join(model_output_dir, "summ_%03d.png" % epoch_idx))
        f.savefig(os.path.join(model_output_dir, "curr_state.png"))
        plt.close() # Close the figures we've opened, otherwise matplotlib complains

        # When we reach an improved state, update histories and do any
        # epoch-level saves that we want to do
        if test_loss < best_loss:
            best_loss = test_loss
            best_epoch = epoch_idx

            # Well also aways want to expose the "best" as
            # model_<idx>_best.pt.  Note: we could symlink it for max
            # space efficiency, but we just save a separate file for
            # (hopefully) a simpler setup.
            best_model_filepath = os.path.join(model_output_dir, "model_%03d_best.pt" % (model_idx))
            torch.save(active_model, best_model_filepath)

            # Disabled by default since results in ~1GB of data for
            # 100 epochs... for just the simple Encoder() model.
            if conf["save_model_history"]:
                model_filepath = os.path.join(model_output_dir, "model_e%03d_l%.0f.pt" % (best_epoch, best_loss))
                torch.save(active_model, model_filepath)

        stop_triggered = early_stopper.record_and_check(test_loss)
        if stop_triggered:
            # Removing temporarily since duplicate, but leaving here
            # for quick ref. of how to add figures to TB
            #
            # this plotting isn't really useful/needed anymore since tb does the same thing
            #fig = plot_loss(train_loss_list, test_loss_list, model_output_dir)
            #tb_writer.add_figure(model_name, fig)
            break

    if conf["generate_training_summ_gif"]:
        gif_filepath = os.path.join(model_output_dir, "model_%03d_training.gif" % model_idx)
        files = glob.glob(model_output_dir + "/summ_*.png") # This will break when we train beyond 100
        files.sort()
        import imageio.v2 as imageio
        with imageio.get_writer(gif_filepath, mode='I', duration = 0.25, loop=1) as gif_writer:
            for fp in files:
                image = imageio.imread(fp)
                gif_writer.append_data(image)

    th.SaveData(model_output_dir)
    tb_writer.close()
    print(f"Best model saved with a loss of {best_loss} at epoch {best_epoch}.")
    # torch.save(active_model, get_save_path())

# Call our main function
if __name__=="__main__":
    main()
