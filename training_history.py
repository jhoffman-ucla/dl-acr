import os
import numpy as np
import pandas as pd

# This implementation is a little overdone, however given that we're using the
# stopping managers to kill our training, we need to be prepared to write beyond
# what we've preallocated, and also have a sense of how big the history actually
# is if it got killed early.
class TrainingHistory():
    size = None # How many histories are we currently storing
    capacity = None # How many histories we have already allocated memory for
    validation_loss = None
    training_loss =  None
    saliency_maps = None

    iter_counter = 0

    def __init__(self, N, dim_x=256, dim_y=256):
        self.size = 0
        self.capacity = N
        self.validation_loss = np.zeros((N), dtype=np.double)
        self.training_loss = np.zeros((N), dtype=np.double)
        self.saliency_maps = np.zeros((N, dim_y, dim_x)) # Not sure if this is the most efficient way to lay out np arrays in memory, just copying torch's layout

    def __iter__(self):
        self.iter_counter = 0
        return self

    def __next__(self):

        if self.iter_counter == self.size:
            raise StopIteration

        val_loss = self.validation_loss[iter_counter]
        train_loss = self.training_loss[iter_counter]
        saliency_map = self.saliency_maps[iter_counter, :, :]
        self.iter_counter += 1

        return (val_loss, training_loss, saliency_map)

    def GetValidationLoss(self):
        return self.validation_loss[:self.size]

    def GetTrainingLoss(self):
        return self.training_loss[:self.size]

    def GetSaliencyMap(self, epoch_idx):
        return self.saliency_maps[epoch_idx, :, :]

    def Record(self, epoch_idx, val_loss, train_loss, saliency_map):

        # This is overbuilt, but basically make sure our size
        # is set to encompass epoch_idx.
        self.size = max(self.size, epoch_idx + 1)

        # Update size if we write outside of existing history
        if epoch_idx >= self.size:
            self.size = epoch_idx

        # If we get epoch_idx beyond current capacity, we need to exted all of our arrays
        # To be moderately efficient, we double the size of the allocation, but keep our
        # size limited to epoch_idx
        if epoch_idx >= self.capacity:
            self.Extend(self.size)

        self.validation_loss[epoch_idx] = val_loss
        self.training_loss[epoch_idx] = train_loss
        self.saliency_maps[epoch_idx, :, :] = saliency_map

    def SaveData(self, output_path):

        epoch_indices = np.arange(0, self.size, 1)

        data = {
            "training_loss": self.training_loss,
            "validation_loss": self.validation_loss,
            }
        
        df = pd.DataFrame(data, index=epoch_indices)
        df.to_json(os.path.join(output_path, "training_results.json"), indent=2)
        df.to_csv(os.path.join(output_path, "training_results.csv"))
        
    def LoadData(self, output_path):
        df = None
        json_path = os.path.join(output_path, "training_results.json")
        if os.path.exists(json_path):
            df = pd.read_json(json_path)
        
        csv_path = os.path.join(output_path, "training_results.csv")
        if os.path.exists(csv_path):
            df = pd.read_json(json_path)

        self.training_loss = [np.array(df["training_loss"]) if df is not None else None]
        self.validation_loss = np.array(df["validation_loss"] if df is not None else None)
        self.size = [len(self.training_loss) if df is not None else None]
        self.saliency_maps = None

        return df
        
    def Extend(self, N):
        print("Extending TrainingHistory capacity by {} records".format(N))
        self.validation_loss = np.append(self.validation_loss, np.zeros((1,N)))
        self.training_loss = np.append(self.training_loss, np.zeros((1,N)))
        dim_1 = self.saliency_maps.shape[1]
        dim_2 = self.saliency_maps.shape[2]
        self.saliency_maps = np.append(self.saliency_maps, np.zeros((N, dim_1, dim_2)), axis=0)
        self.capacity = self.capacity + N

# If called as main, run some simple sanity-check tests
if __name__=="__main__":

    th = TrainingHistory(25)

    if th.size != 0:
        print("Error: expected 0 size but got", th.size)


    if th.capacity != 25:
        print("Error: expected 25 capacity but got", th.size)

    print(th.validation_loss.shape)
    print(th.training_loss.shape)
    print(th.saliency_maps.shape)

    # Expect to trigger Extend
    th.Record(25, 20, 20, np.ones((1, 256, 256)))

    print(th.validation_loss.shape)
    print(th.training_loss.shape)
    print(th.saliency_maps.shape)
