import os
import pandas as pd
from argparse import ArgumentParser

from config import Config
from train import GetModelDirs
from training_history import TrainingHistory

def _total_size(source):
    total_size = os.path.getsize(source)
    for item in os.listdir(source):
        itempath = os.path.join(source, item)
        if os.path.isfile(itempath):
            total_size += os.path.getsize(itempath)
        elif os.path.isdir(itempath):
            total_size += self._total_size(itempath)
    return total_size

def SummarizeModelsDir(models_dir):

    df_summary = pd.DataFrame()
     
    for model_name, model_dirpath in [(d,os.path.join(models_dir,d)) for d in GetModelDirs(models_dir)]:

        conf = Config("")
        conf.Load(os.path.join(model_dirpath, "config.yaml"))
        
        th = TrainingHistory(0)
        df = th.LoadData(model_dirpath)
        
        # Load interesting info about the current model
        interesting_info = {
            "model_name": model_name,
            "model_arch": conf["model_arch"],
            "loss_fn": conf["loss_fn"],
            "epochs": conf["epochs"],
            "batch_size": conf["batch_size"],
            "train_size": conf["training_set_size"],
            "train_add_noise": conf["train_add_noise"],
            "val_size": conf["validation_set_size"],
            "val_add_noise": conf["test_add_noise"],            
            "model_dir": model_dirpath,
            "size_mb": round(_total_size(model_dirpath)/(1000000), 1),
            "learning_rate": conf["learning_rate"],
            "best_val_loss": [df["validation_loss"].min() if df is not None else None],
            "best_train_loss": [df["training_loss"].min() if df is not None else None],
            }

        if len(df_summary.index)==0:
            df_summary = pd.DataFrame(interesting_info)
        else:            
            df_summary.loc[len(df_summary.index)] = interesting_info
        
    return df_summary
        
if __name__ == "__main__":

    parser = ArgumentParser(prog="summarize.py")
    parser.add_argument("models_dir", help = "Directory containing training output.  I.e. the output_dir parameter of your config.")
    parser.add_argument("--csv", dest='csv', default=False, action='store_true')
    parser.add_argument("--json", dest='json', default=False, action='store_true')
    args = parser.parse_args()

    df = SummarizeModelsDir(args.models_dir)

    print(df)

    if args.csv:
        csv_path = os.path.join(args.models_dir, "summary.csv")
        print("Saving csv to: ", csv_path)
        df.to_csv(csv_path)

    if args.json:
        json_path = os.path.join(args.models_dir, "summary.json")
        print("Saving json to: ", json_path)
        df.to_json(json_path)
    
